#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 22:41:16 2022
Saving some relevant LLRF data

@author: ikarpov
"""


import pyjapc
import numpy as np
import os
import time
from datetime import datetime
import argparse



parser = argparse.ArgumentParser(description='Acquisition of 200 MHz TWC LLRF data,\
                                     expect duration of subscription (in seconds)')
parser.add_argument('t', type=int, nargs=1)
args = parser.parse_args()

duration = args.t[0]
print(f"Subscription duration {duration} s.")

japc = pyjapc.PyJapc(noSet=True)

japc.setSelector("SPS.USER.SFTPRO1")

cycle = 'sftpro1'


def myCallback( parameterNames, newValues, headerInfos):
    # timestamps
    t = time.time()
    now = datetime.now()
    date_time = now.strftime('%Y%m%d_%H%M%S')
    tCycle = headerInfos[0]["cycleStamp"]
    #print(parameterNames[:6])
    # Creating dictionaries with data
    vCav = {}
    for i in range(6):
        vCav[f'VcavAmp_cav{i+1}'] = newValues[2*i]['data']
        vCav[f'VcavPhase_cav{i+1}'] = newValues[2*i+1]['data']
    
    offset = len(vCav)
    rfDrive = {}
    for i in range(6):
        rfDrive[f'RFdrivePower_cav{i+1}'] = newValues[3*i+offset]['data']
        rfDrive[f'RFdriveAmp_cav{i+1}'] = newValues[3*i+offset+1]['data']
        rfDrive[f'RFdrivePhase_cav{i+1}'] = newValues[3*i+offset+2]['data']
    
    offset = len(vCav) + len(rfDrive)
    icFwd = {}
    for i in range(6):
        icFwd[f'IcFwdPower_cav{i+1}'] = newValues[2*i+offset]['data']
        icFwd[f'IcFwdPhase_cav{i+1}'] = newValues[2*i+offset+1]['data']
    
    # Adding timestamps
    vCav['timestamp'] = t
    vCav['timecycle'] = tCycle
    
    rfDrive['timestamp'] = t
    rfDrive['timecycle'] = tCycle
    
    icFwd['timestamp'] = t
    icFwd['timecycle'] = tCycle
    
    # Saving data
    np.savez_compressed(f'{fsave}/{cycle}_vcav_LLRF_{date_time}.npz', **vCav)
    np.savez_compressed(f'{fsave}/{cycle}_power_LLRF_{date_time}.npz', **rfDrive)
    np.savez_compressed(f'{fsave}/{cycle}_IcFwd_LLRF_{date_time}.npz', **icFwd)
    
    print(f'Acquisition at {now}')


# List of channels to subscribe
paramsList = []
# Vcav
for i in range(6):
    paramsList.append(f'SA.TWC200_expertVcavAmp.C{i+1}-ACQ/AcquisitionExpert')
    paramsList.append(f'SA.TWC200_expertVcavPhase.C{i+1}-ACQ/AcquisitionExpert')

# RFDrive
for i in range(6):
    paramsList.append(f'SA.TWC200_expertRfDrivePower.C{i+1}-ACQ/AcquisitionExpert')
    paramsList.append(f'SA.TWC200_expertRfDriveAmp.C{i+1}-ACQ/AcquisitionExpert')
    paramsList.append(f'SA.TWC200_expertRfDrivePhase.C{i+1}-ACQ/AcquisitionExpert')

# IcFwd

for i in range(6):
    paramsList.append(f'SA.TWC200_expertIcFwdPower.C{i+1}-ACQ/AcquisitionExpert')
    paramsList.append(f'SA.TWC200_expertIcFwdPhase.C{i+1}-ACQ/AcquisitionExpert')


fsave = '../barrier_bucket'
try:
    os.makedirs(fsave)
    print(f'{fsave} Folder created')
except FileExistsError:
    print(f'{fsave} Folder exists')

print('Ready to subscribe')
print(datetime.now())


japc.subscribeParam(paramsList, myCallback, getHeader=True)

japc.startSubscriptions()

time.sleep(duration)
japc.stopSubscriptions()

print('Done!')
